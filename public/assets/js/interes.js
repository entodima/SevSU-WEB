var intereses = [
    {id: "#food", text: "В еде"},
    {id: "#home", text: "В быту"},
    {id: "#things", text: "В вещах"},
    {id: "#people", text: "В людях"}
]

var interesButton = document.getElementById("interes-button");
var isOpen = false;

function outInteres(){

    for(var i=0; i<intereses.length; i++){
        document.writeln("<p><li class=\"interes-navigation-element\"><a href=\""+intereses[i].id+"\">"+intereses[i].text+"</a></li></p>");
    }
}

interesButton.onclick = function(){
    if(!isOpen) {
        interesButton.parentElement.classList.add("open")
        isOpen = !isOpen
    }
    else {
        interesButton.parentElement.classList.remove("open")
        isOpen = !isOpen
    }
}
