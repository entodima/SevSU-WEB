<?php
include 'app/core/controller.php';
class TestController extends Controller {
    function index(){
        $this->view->render('test.php','Тест', $this->model);
    }

    function send(){
        $this->model->validate($_POST);
        $this->view->render('form_messages/test_message.php', 'Тест - Отправка', $this->model);
    }
}