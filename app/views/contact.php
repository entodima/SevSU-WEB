<link rel="stylesheet" type="text/css" media="screen" href="/public/assets/css/calendar.css"/>
<script src="/public/assets/js/calendar.js"></script>
<script src="/public/assets/js/contact.js"></script>


<div id="contact-main">
    <h1>Отправить письмо</h1>
    <form action="send" method="post" onsubmit="//return validate()">
        <table class="contact-table">
            <tr><td colspan="2"><textarea name="text" id="text" onblur="blurText(this)"></textarea><p class="text-error hided">Добавьте текст</p></td></tr>
            <tr>
                <td>
                    <input class="input-text" name="name" type="text" id="fio" placeholder="Ваше ФИО" onblur="blurName(this)">
                    <p class="text-error hided">Неверный формат</p>
                    <div class="pops hided" id="pops"><p>Иванов Иван Иванович</p></div>
                </td>
                <td style="display:flex">
                    <p>Пол</p>
                    <input type="radio" id="dewey1" name="gender" value="0" />
                        <label for="dewey1">Мужской</label>
                    <input type="radio" id="dewey2" name="gender" value="1" />
                        <label for="dewey2">Женский</label>
                </td>
            </tr>
            <tr>
                <td><input class="input-text" type="email" name="email" placeholder="Ваш e-mail" id="text-email"></td>
                <td rowspan="2">
                    <p>Возраст</p>
                    <div class="calendar-div">
                        <input type="text" id="calendar-text" name="calendar">
                        <div class="calendar-div-MY">
                            <select name="year" id="calendar-year" onchange="updateDate()">
                            </select>
                            <select name="month" id="calendar-month" onchange="updateDate()">
                                <option value="1">Январь</option>
                                <option value="2">Февраль</option>
                                <option value="3">Март</option>
                                <option value="4">Апрель</option>
                                <option value="5">Май</option>
                                <option value="6">Июнь</option>
                                <option value="7">Июль</option>
                                <option value="8">Август</option>
                                <option value="9">Сентябрь</option>
                                <option value="10">Октябрь</option>
                                <option value="11">Ноябрь</option>
                                <option value="12">Декабрь</option>
                            </select>
                        </div>
                            <table id="calendar-table">

                            </table>
                    </div>
                    <script>setDefault()</script>
                </td>
            </tr>
            <tr>
                <td><input class="input-text" type="tel" name="tel" id="phones" placeholder="Ваш телефон" id="text-telephone" onblur="blurPhone(this)"><p class="text-error hided">Неверный формат</p>

                    <div class="pops hided" id="phonespop"><p>Начинается с 3 или 7 и не более 11 символов</p></div>
                </td>
                <td></td>
            </tr>
            <tr>
                <td colspan="2">
                    <input type="submit" value="Отправить" id="button-submit-notactive">
                    <input type="button" value="Сбросить" id="button-reset" onclick="resetBlur()">
                </td>
            </tr>
        </table>
        <div class="hided" id="resetWindow">
            <div>
                <h2>Сбросить данные?</h2>
                <input type="reset" class="buttonsResetSubmit" value="Да">
                <input type="button" class="buttonsResetSubmit" value="Нет">
            </div>
        </div>
    </form>
    <script>setVariables()</script>
</div>

<script src="/public/assets/js/contact_pops.js"></script>