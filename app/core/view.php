<?php /** @noinspection PhpUnusedParameterInspection */

/** @noinspection PhpIncludeInspection */

class View {
    function render($content_view, $title, $model, $layout = 'layout.php'){
        include ('app/views/'.$layout);
    }
}