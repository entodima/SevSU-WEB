<script src="/public/assets/js/photos.js"></script>

<div id="contact-main">
    <div class="hided" id="bigImg" >
        <a onclick="hideImg()">
            <img class="bigImg-Img" src="" alt="" id="bigImg-Img">
        </a>
        <div>
            <div>
                <p onclick="preImg()" id="prevImg">Назад</p>
                <p id="prevNextCapt"></p>
                <p onclick="nextImg()" id="nextImg">Вперед</p>
            </div>
        </div>
    </div>
    <h1>Фотоальбом</h1>
    <table id="photos-table">
        <?php
        $photos_mass = $model->get_photos();
        for ($i=0; $i<count($photos_mass); $i++) {
            echo '<tr id="row'.$i.'">';
            for ($j=0; $j<count($photos_mass[$i]); $j++){
                echo '<td><a onclick="fullImg(this)">
                        <img id="'.$i.','.$j.'" src="'.$photos_mass[$i][$j][1].'" alt="'.$photos_mass[$i][$j][2].'">
                        <p>'.$photos_mass[$i][$j][2].'</p>
                      </a></td>';
            }
            echo '</tr>';
        }
        ?>
    </table>
    <script>//outPhotos();</script>
</div>