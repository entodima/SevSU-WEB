var localStorageTable;
var cookieTable;

function resetLocalStorage(){
    localStorage.clear();
    localStorage.setItem("Интересы","0");
    localStorage.setItem("Учеба","0");
    localStorage.setItem("Контакт","0");
    localStorage.setItem("Фотоальбом","0");
    localStorage.setItem("Тест","0");
    localStorage.setItem("История","0");
}

function saveLocalHistory(){
    localStorage.setItem(window.document.title,(parseInt(localStorage.getItem(window.document.title))+1).toString());
    return
}

function getLocalHistory(){
    localStorageTable = document.getElementById("localStorageTable");

    for(var i=0; i<6; i++){
        switch (i) {
            case 0:
                localStorageTable.innerHTML += "<tr><td>Интересы</td><td>"+localStorage.getItem("Интересы")+"</td></tr>";
                break;
            case 1:
                localStorageTable.innerHTML += "<tr><td>Учеба</td><td>"+localStorage.getItem("Учеба")+"</td></tr>";
                break;
            case 2:
                localStorageTable.innerHTML += "<tr><td>Контакт</td><td>"+localStorage.getItem("Контакт")+"</td></tr>";
                break;
            case 3:
                localStorageTable.innerHTML += "<tr><td>Фотоальбом</td><td>"+localStorage.getItem("Фотоальбом")+"</td></tr>";
                break;
            case 4:
                localStorageTable.innerHTML += "<tr><td>Тест</td><td>"+localStorage.getItem("Тест")+"</td></tr>";
                break;
            case 5:
                localStorageTable.innerHTML += "<tr><td>История</td><td>"+localStorage.getItem("История")+"</td></tr>";
                break;
        }
    }
}


function getCookie(cname) {
  var name = cname + "=";
  var decodedCookie = decodeURIComponent(document.cookie);
  var ca = decodedCookie.split(';');
  for(var i = 0; i <ca.length; i++) {
    var c = ca[i];
    while (c.charAt(0) == ' ') {
      c = c.substring(1);
    }
    if (c.indexOf(name) == 0) {
      return c.substring(name.length, c.length);
    }
  }
  return "";
}

function setCookie(cname, cvalue, exdays) {
  var d = new Date();
  d.setTime(d.getTime() + (exdays*24*60*60*1000));
  var expires = "expires="+ d.toUTCString();
  document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

function resetCookies(){ 
    if(getCookie("/interes")=="") setCookie("/interes",0,30)
    if(getCookie("/studies")=="") setCookie("/studies",0,30)
    if(getCookie("/contact")=="") setCookie("/contact",0,30)
    if(getCookie("/photos")=="") setCookie("/photos",0,30)
    if(getCookie("/test")=="") setCookie("/test",0,30)
    if(getCookie("/history")=="") setCookie("/history",0,30)
    return
}

function saveCookieHistory(){
    setCookie(window.location.pathname.split("/").pop(),(parseInt(getCookie(window.location.pathname.split("/").pop()))+1).toString());
    return
}

function getCookieHistory(){
    cookieTable = document.getElementById("cookieTable");

    for(var i=0; i<6; i++){
        switch (i) {
            case 0:
                cookieTable.innerHTML += "<tr><td>Интересы</td><td>"+getCookie("/interes")+"</td></tr>";
                break;
            case 1:
                cookieTable.innerHTML += "<tr><td>Учеба</td><td>"+getCookie("/studies")+"</td></tr>";
                break;
            case 2:
                cookieTable.innerHTML += "<tr><td>Контакт</td><td>"+getCookie("/contact")+"</td></tr>";
                break;
            case 3:
                cookieTable.innerHTML += "<tr><td>Фотоальбом</td><td>"+getCookie("/photos")+"</td></tr>";
                break;
            case 4:
                cookieTable.innerHTML += "<tr><td>Тест</td><td>"+getCookie("/test")+"</td></tr>";
                break;
            case 5:
                cookieTable.innerHTML += "<tr><td>История</td><td>"+getCookie("/history")+"</td></tr>";
                break;
        }
    }
}
