var prevImg = [
    {name: "interes.html", imgOpen: "/public/assets/img/menu/open/info.png", imgNotopen: "/public/assets/img/menu/notopen/info.png", open: false},
    {name: "studies.php", imgOpen: "/public/assets/img/menu/open/stud.png", imgNotopen: "/public/assets/img/menu/notopen/stud.png", open: false},
    {name: "contact.html", imgOpen: "/public/assets/img/menu/open/cont.png", imgNotopen: "/public/assets/img/menu/notopen/cont.png", open: false},
    {name: "photos.html", imgOpen: "/public/assets/img/menu/open/phot.png", imgNotopen: "/public/assets/img/menu/notopen/phot.png", open: false},
    {name: "test.html", imgOpen: "/public/assets/img/menu/open/test.png", imgNotopen: "/public/assets/img/menu/notopen/test.png", open: false},
    {name: "history.html", imgOpen: "/public/assets/img/menu/open/hist.png", imgNotopen: "/public/assets/img/menu/notopen/hist.png", open: false},
]

function addPrevImg(){
    for(var i=0; i<prevImg.length; i++){
        if(prevImg[i].name == window.location.pathname.split("/").pop()){
            document.getElementById(i).src = prevImg[i].imgOpen;
        } else document.getElementById(i).src = prevImg[i].imgNotopen;
    }
}

function changeImg(mouseIn, element){
    if (mouseIn) {  //мышь входит
        element.children[0].children[0].src = prevImg[element.children[0].children[0].id].imgOpen;
    } else {        //мышь выходит
        element.children[0].children[0].src = prevImg[element.children[0].children[0].id].imgNotopen;
    }
}
