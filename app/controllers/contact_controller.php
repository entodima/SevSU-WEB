<?php
include 'app/core/controller.php';
class ContactController extends Controller {
    function index(){
        $this->view->render('contact.php','Контакт',$this->model);
    }

    function send(){
        $this->model->validate($_POST);
        $this->view->render('form_messages/contact_message.php', 'Контакт - Отправка', $this->model);
    }
}