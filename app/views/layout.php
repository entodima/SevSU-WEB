<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8" />
    <title><?=$title?></title>
    <link rel="stylesheet" type="text/css" href="/public/assets/css/main.css" />
    <script src="/public/assets/js/plugins/jquery-3.3.1.js"></script>
</head>



<body class="index">
<header>
    <div class="panel" id="panel">
        <a href="/"><img src="/public/assets/img/squashed/logo.png" alt="logo"></a>
        <a href="/contact/"><p>Контакты</p></a>
        <a href="/interes/"><p>Интересы</p></a>
        <a href="/photos/"><p>Фотографии</p></a>
        <a href="/studies/"><p>Учеба</p></a>
        <a href="/test/"><p>Тест</p></a>
    </div>
    <script src="/public/assets/js/panel.js"></script>
</header>

<div class="main">
    <?php
    /** @noinspection PhpIncludeInspection */
    include "app/views/".$content_view;
    ?>
</div>
</body>
</html>