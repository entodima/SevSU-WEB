let calendar_calendarText;
let dname;
let tel;


var isAllTrue = [false,false,false];

var buttonSubmit;

function validate(){
    if(checkText(calendar_calendarText) && checkName(name) && checkTel(tel)) return true
    else return false
}

function setVariables(){
    calendar_calendarText = document.forms[0].text;
    dname = document.forms[0].name;
    tel = document.forms[0].tel;
    buttonSubmit = document.getElementById("button-submit-notactive")
}

function blurText(element){
    if(element.value=="") {
        element.classList.add("red-border");
        element.parentElement.children[1].classList.remove("hided");
        element.classList.remove("green-border");
        isAllTrue[0]=false;
    } else {
        element.classList.remove("red-border");
        element.parentElement.children[1].classList.add("hided");
        element.classList.add("green-border");
        isAllTrue[0]=true;
    }
    turnOnButton();
}

function blurName(element){
    if(element.value == ""){
        element.classList.remove("red-border");
        element.classList.remove("green-border");
        element.parentElement.children[1].classList.add("hided");
        isAllTrue[1]=true;
    } else{
        if(!checkName(element)){
            element.classList.add("red-border");
            element.parentElement.children[1].classList.remove("hided");
            element.classList.remove("green-border");
            isAllTrue[1]=false;
        } else {
            element.classList.remove("red-border")
            element.parentElement.children[1].classList.add("hided");
            element.classList.add("green-border");
            isAllTrue[1]=true;
        }
    }
    turnOnButton();
}

function blurPhone(element){
    if(element.value==""){
        isAllTrue[2]=true;
        element.classList.remove("red-border");
        element.classList.remove("green-border");
        element.parentElement.children[1].classList.add("hided")
    } else {
        if(!checkText(element)){
            element.classList.add("red-border");
            element.parentElement.children[1].classList.remove("hided");
            element.classList.remove("green-border");
            isAllTrue[2]=false
        } else {
            isAllTrue[2]=true;
            element.classList.remove("red-border");
            element.parentElement.children[1].classList.add("hided");
            element.classList.add("green-border")
        }
    }
    turnOnButton();
}

function checkText(text){
    return text.vaue == "";
}

function checkName(name){
    var value = name.value;

    var nameS = /^[А-ЯЁ][а-яё]+ [А-ЯЁ][а-яё]+ [А-ЯЁ][а-яё]+$/;

    if (value.search(nameS) == -1){
        //alert("ФИО введено неверно");
        return false;
    } else {
        return true
    }
}

function resetBlur(){
    calendar_calendarText.classList.remove("green-border");
    calendar_calendarText.classList.remove("red-border");
    calendar_calendarText.parentElement.children[1].classList.add("hided");
    dname.classList.remove("green-border");
    dname.classList.remove("red-border");
    dname.parentElement.children[1].classList.add("hided");
    tel.classList.remove("green-border");
    tel.classList.remove("red-border");
    tel.parentElement.children[1].classList.add("hided");
    for(var i=0; i<isAllTrue.length; i++) isAllTrue[i] = false
}

function turnOnButton(){
    console.log(isAllTrue);
    var all = true;
    for(var i=0; i<isAllTrue.length; i++) if(!isAllTrue[i]) all = false;
    if(all) buttonSubmit.id = "button-submit";
    else buttonSubmit.id = "button-submit-notactive";
}