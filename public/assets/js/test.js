function validate(){
    var name = document.forms[0].name
    var radios = document.getElementsByName("drone")

    if (checkName(name) && checkRadio(radios)) return true;
    else return false;
}

function checkName(name){
    var value = name.value;

    var nameS = /^[А-ЯЁ][а-яё]+ [А-ЯЁ][а-яё]+ [А-ЯЁ][а-яё]+$/;

    if (value.search(nameS) == -1){
        //alert("ФИО введено неверно");
        name.focus();
        name.classList.add("red-border");   
        return false;
    } else {
        name.classList.remove("red-border")
        return true
    }
}

function checkRadio(radios){
    
    for(i=0; i<radios.length; i++){
        if (radios[i].checked){
            return true;
        }
    }
    alert("Выберите хотя бы один ответ во втором вопросе")
    return false
}