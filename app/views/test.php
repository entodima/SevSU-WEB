
<script src="/public/assets/js/test.js"></script>


<div id="contact-main">

    <h1>Тест "Физика"</h1>
    <form action="send" method="post" onsubmit="//return validate()">
            <table class="contact-table">
                <tr>
                    <td style="display:flex" colspan="2">
                        <p >Сила тока - это физическая величина, равная...</p>
                        <input type="text" name="input1">
                    </td>
                </tr>
                <tr>
                    <td style="display:flex"  colspan="2">
                        <p style="margin-right: 10px">Как называется единица силы тока?</p>
                        <input style="margin-right: 5px" type="radio" id="dewey1" name="input2" value="Джоуль" />
                            <label   style="margin-right: 5px" for="dewey1">Джоуль</label>
                        <input style="margin-right: 5px" type="radio" id="dewey2" name="input2" value="Кулон" />
                            <label  style="margin-right: 5px"  for="dewey2">Кулон</label>
                        <input style="margin-right: 5px" type="radio" id="dewey3" name="input2" value="Ватт" />
                            <label  style="margin-right: 5px"  for="dewey3">Ватт</label>
                        <input style="margin-right: 5px" type="radio" id="dewey4" name="input2" value="Ампер" />
                            <label for="dewey4">Ампер</label>
                    </td>
                </tr>

                <tr>
                    <td style="display:flex"  colspan="2">
                        <p style="margin-right:10px">Магнитный поток, как физическая величина характеризуется...</p>
                        <select name="input3">
                            <optgroup label="1">
                                <option value="0">Направлением линий магнитной индукции</option>
                            </optgroup>
                            <optgroup label="2">
                                <option value="1">Количеством линий магнитной индукции</option>
                            </optgroup>
                            <optgroup label="3">
                                <option value="2">Количеством линий магнитной индукции, пронизывающих контур</option>
                            </optgroup>
                            <optgroup label="4">
                                <option value="3">Среди ответов нет верного</option>
                            </optgroup>
                        </select>
                    </td>
                </tr>

                <tr>
                    <td><input name="name" placeholder="Ваше ФИО" id="text-name"></td>
                    <td style="display:flex">
                        <p style="margin-right:10px">Группа</p>
                        <select name="input4">
                            <optgroup label="ИС">
                                <option value="is-31">ИС/б-31-о</option>
                                <option value="is-32">ИС/б-32-о</option>
                                <option value="is-33">ИС/б-33-о</option>
                            </optgroup>
                            <optgroup label="ИВТ">
                                <option value="ivt-31">ИВТ/б-31-о</option>
                                <option value="ivt-32">ИВТ/б-32-о</option>
                            </optgroup>
                        </select>
                    </td>
                </tr>

                <tr>
                    <td colspan="2">
                        <input type="submit" value="Отправить" id="button-submit">
                        <input type="reset" value="Сбросить" id="button-reset">
                    </td>
                </tr>
            </table>
        </form>

</div>