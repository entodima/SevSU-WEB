<?php
class FormValidation {
    public $is_valid = true;
    protected $rules = []; //[поле, значение, тип(0-str, 1-int, 2-arr, 3-email)]
    protected $errors = [];

    public function validate($post_array){
        foreach ($this->rules as $rule){
            $this->is_empty($post_array[$rule[0]], $rule[0]);
            if ($rule[2]==1){
                $this->is_integer($post_array[$rule[0]], $rule[0]);
            }
            if ($rule[2]==3){
                $this->is_email($post_array[$rule[0]], $rule[0]);
            }
        }
    }

    public function set_rule($field_name, $validator_name, $type_name){
        array_push($this->rules, [$field_name, $validator_name, $type_name]);
    }

    public function show_rules(){
        echo '<h2>Rules: </h2>';
        foreach ($this->rules as $item){
            echo '<p>' .$item[0].' - '.$item[1].'</p><br>';
        }
    }

    public function show_errors(){
        echo '<h2>Errors: </h2>';
        foreach ($this->errors as $error){
            echo '<p>'.$error.'</p><br>';
        }
    }

    public function is_empty($string, $rule){
        if (strlen($string) == 0){
            array_push($this->errors, $rule.' - is Empty');
            $this->is_valid = false;
        };
    }

    public function is_integer($string, $rule){
        if (!filter_var($string, FILTER_VALIDATE_INT)){
            array_push($this->errors, $rule.' - is not Integer');
            $this->is_valid = false;
        };
    }

    public function is_email($string, $rule){
        if (!filter_var($string, FILTER_VALIDATE_EMAIL)){
            array_push($this->errors, $rule.' - is not Email');
            $this->is_valid = false;
        }
    }
}