<?php
include "app/models/validators/contact_verification.php";
include 'app/core/model.php';
class ContactModel extends Model {
    function __construct() {
        parent::__construct();
        //0-str, 1-int, 2-arr, 3-email, 4-date
        $this->validator = new ContactVerification();

        $this->validator->set_rule('text','', 0);
        $this->validator->set_rule('name','', 0);
        $this->validator->set_rule('email','', 3);
        $this->validator->set_rule('gender', [0,1], 2);
        $this->validator->set_rule('tel','', 1);
        $this->validator->set_rule('calendar','', 4);
    }
}