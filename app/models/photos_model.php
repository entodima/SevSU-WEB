<?php
class PhotosModel  {
    private $photos = [
        [   ["/public/assets/img/1.jpg", "/public/assets/img/squashed/1.jpg",  "Bladerunner 2049"],
            ["/public/assets/img/2.png", "/public/assets/img/squashed/2.png", "Bladerunner 2050",],
            ["/public/assets/img/3.png", "/public/assets/img/squashed/3.png", "Bladerunner 2051",],
            ["/public/assets/img/4.jpg", "/public/assets/img/squashed/4.jpg", "Bladerunner 2052",],
            ["/public/assets/img/5.jpg", "/public/assets/img/squashed/5.jpg", "Bladerunner 2053"],],

        [   ["/public/assets/img/6.png", "/public/assets/img/squashed/6.png",  "Bladerunner 2054",],
            ["/public/assets/img/7.jpg", "/public/assets/img/squashed/7.jpg", "Bladerunner 2055",],
            ["/public/assets/img/8.jpg", "/public/assets/img/squashed/8.jpg", "Bladerunner 2056",],
            ["/public/assets/img/9.jpg", "/public/assets/img/squashed/9.jpg", "Bladerunner 2057",],
            ["/public/assets/img/10.jpg", "/public/assets/img/squashed/10.jpg", "Bladerunner 2058"],],

        [   ["/public/assets/img/11.jpg", "/public/assets/img/squashed/11.jpg",  "Bladerunner 2059",],
            ["/public/assets/img/12.jpg", "/public/assets/img/squashed/12.jpg", "Bladerunner 2060",],
            ["/public/assets/img/13.jpg", "/public/assets/img/squashed/13.jpg", "Bladerunner 2061",],
            ["/public/assets/img/14.png", "/public/assets/img/squashed/14.png", "Bladerunner 2062",],
            ["/public/assets/img/15.jpg", "/public/assets/img/squashed/15.jpg", "Bladerunner 2063"],],
    ];

    public function __construct() {

    }
    public function test(){
        echo 'TEST';
    }
    public function get_photos(){
        return $this->photos;
    }
}
