<?php
include "app/models/validators/test_verification.php";
include 'app/core/model.php';
class TestModel extends Model {
    function __construct() {
        parent::__construct();
        //0-str, 1-int, 2-arr, 3-email
        $this->validator = new TestVerification();

        $this->validator->set_rule('input1','отношению заряда ко времени', 0);
        $this->validator->set_rule('input2','Ампер', 0);
        $this->validator->set_rule('input3',3, 1);
        $this->validator->set_rule('input4',['is-31','is-32','is-33','ivt-31','ivt-32'], 2);
        $this->validator->set_rule('name','', 0);
    }
}