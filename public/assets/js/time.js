var d1 = new Date();
var timeButton = document.getElementById("timeButton");
var time = document.getElementById("time");

var d0= new Date(d1.getFullYear(), 0 ,1);

window.setInterval(function(){ 
    time.innerHTML = d1.getDate()+"."+d1.getMonth()+"."+d1.getFullYear()+" "+((d1.getTime() - d0.getTime()) / (1000*60*60*24*7)).toFixed();
}, 1000);

