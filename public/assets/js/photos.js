var bigImg;
var path;
var i;
var id=1;
var idI = 0;
var idJ = 0;

var photos = [
    [   {link: "/public/assets/img/1.jpg", small:"/public/assets/img/squashed/1.jpg", title: "Bladerunner 2049"}, 
        {link: "/public/assets/img/2.png", small:"/public/assets/img/squashed/2.png", title:"Bladerunner 2050"},
        {link: "/public/assets/img/3.png", small:"/public/assets/img/squashed/3.png", title:"Bladerunner 2051"},
        {link: "/public/assets/img/4.jpg", small:"/public/assets/img/squashed/4.jpg", title:"Bladerunner 2052"},
        {link: "/public/assets/img/5.jpg", small:"/public/assets/img/squashed/5.jpg", title:"Bladerunner 2053"}],

    [   {link: "/public/assets/img/6.png", small:"/public/assets/img/squashed/6.png", title: "Bladerunner 2054"}, 
        {link: "/public/assets/img/7.jpg", small:"/public/assets/img/squashed/7.jpg", title:"Bladerunner 2055"},
        {link: "/public/assets/img/8.jpg", small:"/public/assets/img/squashed/8.jpg", title:"Bladerunner 2056"},
        {link: "/public/assets/img/9.jpg", small:"/public/assets/img/squashed/9.jpg", title:"Bladerunner 2057"},
        {link: "/public/assets/img/10.jpg", small:"/public/assets/img/squashed/10.jpg", title:"Bladerunner 2058"}],

    [   {link: "/public/assets/img/11.jpg", small:"/public/assets/img/squashed/11.jpg", title: "Bladerunner 2059"}, 
        {link: "/public/assets/img/12.jpg", small:"/public/assets/img/squashed/12.jpg", title:"Bladerunner 2060"},
        {link: "/public/assets/img/13.jpg", small:"/public/assets/img/squashed/13.jpg", title:"Bladerunner 2061"},
        {link: "/public/assets/img/14.png", small:"/public/assets/img/squashed/14.png", title:"Bladerunner 2062"},
        {link: "/public/assets/img/15.jpg", small:"/public/assets/img/squashed/15.jpg", title:"Bladerunner 2063"}]
]

var summ = photos.length * photos[0].length;

function outPhotos() { 
    photosTable = document.getElementById("photos-table")
    var j = 0;

    idI=0;
    for(var i=0; i<photos.length; i++){
        photosTable.innerHTML += "<tr id=\"row"+i+"\">";
        
        idJ=0;
        for(var i1=0; i1<photos[i].length; i1++){
            document.getElementById("row"+i).innerHTML += "<td><a onclick=\"fullImg(this)\"><img id=\""+idI+","+idJ+"\" src=\""+photos[i][i1].small+"\"><p>"+photos[i][i1].title+"<p></a></td>";
            j++;
            idJ++;
        }
        idI++;
    }
}

function fullImg(element){
    bigImg = document.getElementById("bigImg");
    idI = element.children[0].id.split(',')[0];
    idJ = element.children[0].id.split(',')[1];
    path = photos[idI][idJ].link;
    //console.log(photos[idI][idJ].link)
    
    bigImg.children[0].children[0].src = photos[idI][idJ].link;

    if(idI==0 && idJ==0) $("#prevImg").hide();
    id = getIdElement(idI, idJ)
    $("#prevNextCapt").html("Фото "+id+" из "+summ);
    $("#bigImg").fadeIn(200);

    if(idI>0 || idJ>0) $("#prevImg").show();
    if(idI==photos.length-1 && idJ==photos[0].length-1) $("#nextImg").hide();
    if(idJ<photos[0].length-1)$("#nextImg").show();
}
function hideImg(){
    $("#bigImg").fadeOut(200);
}

function nextImg(){
    incId(idI, idJ)
    $(".bigImg-Img").fadeOut(300, function(){
        bigImg.children[0].children[0].src = photos[idI][idJ].link;
        $(".bigImg-Img").fadeIn(300);
        id = getIdElement(idI, idJ)
        $("#prevNextCapt").html("Фото "+id+" из "+summ);
    });
    if(idI>0 || idJ>0) $("#prevImg").fadeIn(300);
    if(idI==photos.length-1 && idJ==photos[0].length-1) $("#nextImg").fadeOut(300);
}

function preImg(){
    decrId(idI,idJ);
    $(".bigImg-Img").fadeOut(300, function(){
        bigImg.children[0].children[0].src = photos[idI][idJ].link;
        $(".bigImg-Img").fadeIn(300);
        id = getIdElement(idI, idJ)
        $("#prevNextCapt").html("Фото "+id+" из "+summ);
    });
    if(idI>0 && idJ<photos[0].length-1) $("#nextImg").fadeIn(300);
    if(idI==0 && idJ==0) $("#prevImg").fadeOut(300);
}

function incId(i, j){
    if(j==photos[0].length-1){
        if(i!=photos.length-1){
            idJ=0;
            idI++;
            id++;
        }
    } else {
        idJ++;
        id++;
    }
}
function decrId(i,j){
    if(idJ==0){
        if(idI>0){
            idJ = photos[0].length-1;
            idI--;
            id--;
        }
    } else {
        idJ--;
        id--;
    }
}
function getIdElement(i,j){
    var temp = 0;

    for(var ii = 0; ii<photos.length; ii++){
        for (var jj=0; jj<photos[0].length; jj++){
            temp++;
            if(ii==i && jj==j) return temp;
        }
    }
    
}